let Boards = localStorage.getItem('BordersList');
let BoardCount = localStorage.getItem('BoardCount');
let MaxID = JSON.parse(localStorage.getItem('MaxID'));

window.onload = function name() {
    Boards = Boards? JSON.parse(Boards) :[];
    localStorage.setItem('BordersList',JSON.stringify(Boards));

    BoardCount = BoardCount? JSON.parse(BoardCount) :0;
    localStorage.setItem('BoardCount',JSON.stringify(BoardCount));
    MaxID = MaxID? JSON.parse(MaxID):0;
    localStorage.setItem('MaxID',JSON.stringify(MaxID));
    document.getElementById('notes').innerHTML="";

    showBoards(Boards);
}


document.getElementById("notes").style.zIndex = "0";
let colors = {color1:'#d6eadf', 
color2 :'#b8e0d2',
 color3 :'#95b8d1',
  color4 :'#809bce'}

let notes = document.getElementById("notes");

function newBoard() {
    document.getElementById('modal-id').style.display='block';
}

function closeModal() {
    document.getElementById('modal-id').style.display='none';
    document.getElementById('board-title').value ="";
}

function addBoard() {

    let boardTitle = document.getElementById('board-title').value;
    if (boardTitle != "") {

        Boards.push({
            id:BoardCount,
            title:boardTitle,
            createDate:today(),
            note:[]
        });
                
        localStorage.setItem('BordersList',JSON.stringify(Boards));
        document.getElementById('modal-id').style.display='none';
        document.getElementById('board-title').value ="";
        showBoards(Boards);
        document.getElementById('notes').innerHTML="";
        active(BoardCount);
        BoardCount++;
        localStorage.setItem('BoardCount',JSON.stringify(BoardCount));

    } else {
       alert("pleace add Board Title"); 
    }
}

function showBoards(boardList) {
    document.getElementById("board-list").innerHTML="";
    boardList.forEach(boardItem => {
       document.getElementById("board-list").innerHTML += showBoard(boardItem);
    });
}

function showBoard(boardItem) {
    return `
    <div class="tap" onclick="active('${boardItem.id}')" id="board-${boardItem.id}">
        <h1>${boardItem.title}</h1>
        <span class="close" title="delete" onclick="deleteBoard('${boardItem.id}')">&times;</span>
    </div>`;
}

function today() {
    let day = new Date();
    let dd = String(day.getDate()).padStart(2, '0');
    let mm = String(day.getMonth() + 1).padStart(2, '0');
    let yyyy = day.getFullYear();
    day = mm + '/' + dd + '/' + yyyy;

    return day;
}

function deleteBoard(id) {
  Boards.splice(function (board) {
    board.id == id;
  },1);
  localStorage.setItem('BordersList',JSON.stringify(Boards));
  showBoards(Boards);
  document.getElementById('notes').innerHTML="";
  document.getElementById('add-note').style.display='none';

}
    

function closeNote(note) {
  Notsestorge.splice((Note) => {
    Note.id !== id;
  },1);
  localStorage.setItem('notelist',JSON.stringify(Notsestorge));
  note.style.display = "none";
}


function newNote(id) {

  let board = Boards.find(function (item) {
      return item.id == id;
  });
  MaxID++;
  board.note.push({
    id:MaxID,
    description:"",
    color:"#d6eadf"
  });
  let Note = document.createElement("div");
  Note.innerHTML = addnote(MaxID,board.id);
  localStorage.setItem('BordersList',JSON.stringify(Boards));
  addtoborde(Note);
}

function addnote(noteID,boardID) {
  let top = Math.floor(Math.random() * Math.floor(700));
  let left = Math.floor(Math.random() * Math.floor(1600));

  let note = ` <div class="note" id="note" style="top: ${top}px; left: ${left}px" onclick="myfunction()">
      <div class="bar" onmousedown="move()"></div>
      <textarea id="note-${noteID}" onfocusout="editNote('${noteID}','${boardID}')"></textarea> 
      <div class="note-btn">
        <div class="noteColor">
          <button class="btn1" onclick="changecolor('color1', this.parentElement.parentElement.parentElement)" > </button>
          <button class="btn2" onclick="changecolor('color2', this.parentElement.parentElement.parentElement)"> </button>
          <button class="btn3" onclick="changecolor('color3', this.parentElement.parentElement.parentElement)"> </button>
          <button class="btn4" onclick="changecolor('color4', this.parentElement.parentElement.parentElement)"> </button>
        </div>
        <span class="cancel" id="closeNote" onclick="closeNote(this.parentElement.parentElement)" >X</span>
      </div>   
        
      </div>`
      
  return note;
 
}

function move() {
  onDragStart(new MouseEvent("mousedown"));
}
function addtoborde(Note) {
  notes.innerHTML += Note.innerHTML;
}
function changecolor(color,note) {
  
  switch (color) {
    case 'color1' :
        note.firstElementChild.style.backgroundColor=colors.color1;
        break;
    case 'color2':
        note.firstElementChild.style.backgroundColor=colors.color2;
        break;
    case 'color3':
        note.firstElementChild.style.backgroundColor=colors.color3;
        break;   
    case 'color4':
        note.firstElementChild.style.backgroundColor=colors.color4;
        break; 
  }
  
}
function myfunction() {
  document.getElementById("note").style.zIndex = "1";
}

function active(id) {
  
  let elements = document.querySelectorAll(".tap");
  elements.forEach(function (element) {
    element.style.backgroundColor='#ddd';
  });
  let idBoard = "board-"+id;
  let boardHTML = document.getElementById(idBoard);
  boardHTML.style.backgroundColor='#fff';
  document.getElementById('add-note').style.display='flex';
  document.getElementById('add-note').addEventListener('click',function() {
    newNote(id);
  });

  let board = Boards.find(function (item) {
      return item.id == id;
  });
  document.getElementById('notes').innerHTML="";
  board.note.forEach(function (item) {
    document.getElementById('notes').innerHTML += addnote(item,id);
  });
  
}

function editNote(id,boardID) {
  Boards.forEach(function (board) {
      if(board.id == boardID){
        board.note.forEach(function (item) {
          if (item.id == id) {
              item.description = document.getElementById('note-'+id).value; 
          }
        });
      }
  });

  localStorage.setItem('BordersList',JSON.stringify(Boards));
}


  let draggedEl,grabPointY,grabPointX;
  
   function  onDragStart (ev) {
    let boundingClientRect;
    if ((ev.target).className.indexOf('bar') === -1) {
      return;
    }
    
    draggedEl = this;
    
    boundingClientRect = draggedEl.getBoundingClientRect();
    
    grabPointY = boundingClientRect.top - ev.clientY;
    grabPointX = boundingClientRect.left - ev.clientX;
  }
  
    function onDrag(ev) {
    if (!draggedEl) {
      return;
    }
    
    var posX = ev.clientX + grabPointX,
        posY = ev.clientY + grabPointY;
    
    if (posX < 0) {
      posX = 0;
    }
    
    if (posY < 0) {
      posY = 0;
    }
    
    draggedEl.style.transform = "translateX(" + posX + "px) translateY(" + posY + "px)";
  }
  
function onDragEnd() {
  draggedEl = null;
  grabPointX = null;
  grabPointY = null;
}
document.addEventListener('mousemove',onDrag,false);
document.addEventListener('mouseup',onDragEnd,false);


